-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema tiendaonline
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema tiendaonline
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tiendaonline` DEFAULT CHARACTER SET utf8 ;
USE `tiendaonline` ;

-- -----------------------------------------------------
-- Table `tiendaonline`.`producto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendaonline`.`producto` (
  `idProducto` INT(11) NOT NULL AUTO_INCREMENT,
  `nombreProducto` VARCHAR(45) NOT NULL,
  `valorCompra` DOUBLE NOT NULL,
  `valorVenta` DOUBLE NOT NULL,
  `cantidad` DOUBLE NOT NULL,
  PRIMARY KEY (`idProducto`),
  UNIQUE INDEX `idProducto_UNIQUE` (`idProducto` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tiendaonline`.`transaccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendaonline`.`transaccion` (
  `idTransaccion` INT(11) NOT NULL AUTO_INCREMENT,
  `tipoTransaccion` VARCHAR(45) NOT NULL,
  `fechaTransaccion` VARCHAR(45) NOT NULL,
  `vendedor` VARCHAR(45) NOT NULL,
  `comprador` VARCHAR(45) NOT NULL,
  `total` DOUBLE NOT NULL,
  PRIMARY KEY (`idTransaccion`),
  UNIQUE INDEX `idTransaccion_UNIQUE` (`idTransaccion` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tiendaonline`.`detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tiendaonline`.`detalle` (
  `idDetalle` INT(11) NOT NULL AUTO_INCREMENT,
  `valorDetalle` DOUBLE NOT NULL,
  `cantidadDetalle` DOUBLE NOT NULL,
  `totalDetalle` DOUBLE NOT NULL,
  `idProducto` INT(11) NOT NULL,
  `idTransaccion` INT(11) NOT NULL,
  PRIMARY KEY (`idDetalle`),
  UNIQUE INDEX `idDetalle_UNIQUE` (`idDetalle` ASC) VISIBLE,
  INDEX `idProducto_idx` (`idProducto` ASC) VISIBLE,
  INDEX `idTransaccion_idx` (`idTransaccion` ASC) VISIBLE,
  CONSTRAINT `idProducto`
    FOREIGN KEY (`idProducto`)
    REFERENCES `tiendaonline`.`producto` (`idProducto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `idTransaccion`
    FOREIGN KEY (`idTransaccion`)
    REFERENCES `tiendaonline`.`transaccion` (`idTransaccion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
